const dataPenjualanNovel = [
  {
    idProduct: "BOOK002421",
    namaProduk: "Pulang - Pergi",
    penulis: "Tere Liye",
    hargaBeli: 60000,
    hargaJual: 86000,
    totalTerjual: 150,
    sisaStok: 17,
  },
  {
    idProduct: "BOOK002351",
    namaProduk: "Selamat Tinggal",
    penulis: "Tere Liye",
    hargaBeli: 75000,
    hargaJual: 103000,
    totalTerjual: 171,
    sisaStok: 20,
  },
  {
    idProduct: "BOOK002941",
    namaProduk: "Garis Waktu",
    penulis: "Fiersa Besari",
    hargaBeli: 67000,
    hargaJual: 99000,
    totalTerjual: 213,
    sisaStok: 5,
  },
  {
    idProduct: "BOOK002941",
    namaProduk: "Laskar Pelangi",
    penulis: "Andrea Hirata",
    hargaBeli: 55000,
    hargaJual: 68000,
    totalTerjual: 20,
    sisaStok: 56,
  },
];

function getInfoPenjualan(dataPenjualan) {
  if (!Array.isArray(dataPenjualan)) {
    return "Error input must be an array";
  }

  let totalPendapatan = 0;
  let totalModal = 0;
  let bukuTerjual = 0;
  let bukuTerlaris;
  let penulisTerlaris;

  for (let i = 0; i < dataPenjualan.length; i++) {
    //mengitung total pendapatan
    totalPendapatan +=
      dataPenjualan[i].hargaJual * dataPenjualan[i].totalTerjual;

    //menghitung total modal
    totalModal +=
      dataPenjualan[i].hargaBeli *
      (dataPenjualan[i].totalTerjual + dataPenjualan[i].sisaStok);

    //produk Buku Terlaris && penulis terlaris
    if (dataPenjualan[i].totalTerjual > bukuTerjual) {
      bukuTerjual = dataPenjualan[i].totalTerjual;
      bukuTerlaris = dataPenjualan[i].namaProduk;
      penulisTerlaris = dataPenjualan[i].penulis;
    }
  }
  // total keuntungan
  let totalKeuntungan = totalPendapatan - totalModal;
  let totalKeuntunganRupiah = totalKeuntungan.toLocaleString("id-ID", {
    style: "currency",
    currency: "IDR",
  });
  let totalModalRupiah = totalModal.toLocaleString("id-ID", {
    style: "currency",
    currency: "IDR",
  });
  //precetase keuntungan
  let persentaseKeuntungan = (totalKeuntungan / totalModal) * 100;

  let infoPenjualan = {
    totalKeuntungan: totalKeuntunganRupiah,
    totalModal: totalModalRupiah,
    persentaseKeuntungan: `${persentaseKeuntungan.toFixed(2)}%`,
    produkBukuTerlaris: bukuTerlaris,
    penulisTerlaris: penulisTerlaris,
  };
  return infoPenjualan;
}

console.log(getInfoPenjualan(dataPenjualanNovel));
