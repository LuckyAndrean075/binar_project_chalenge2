function getAngkaTerbesarKedua(dataNumbers) {
  if (typeof dataNumbers === "undefined") {
    return "Error input cannot be empty";
  }

  if (!Array.isArray(dataNumbers)) {
    return "Error input must be an array";
  }

  if (dataNumbers.length < 2) {
    return "Error Array lenght should be 2 or longer";
  }

  let largestNumber = 0;
  let SecondNumber = 0;

  for (let i = 0; i < dataNumbers.length; i++) {
    if (dataNumbers[i] > largestNumber) {
      largestNumber = dataNumbers[i];
    } else if (
      dataNumbers[i] !== largestNumber &&
      dataNumbers[i] > SecondNumber
    ) {
      SecondNumber = dataNumbers[i];
    }
  }

  return SecondNumber;
}

const dataNumbers = [9, 4, 7, 7, 4, 3, 2, 2, 8];
console.log(getAngkaTerbesarKedua(dataNumbers));
console.log(getAngkaTerbesarKedua(1, 2));
console.log(getAngkaTerbesarKedua(0));
console.log(getAngkaTerbesarKedua());
