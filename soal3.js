function checkEmail(email) {
  //check input
  if (typeof email === "undefined") {
    return "Error : Input is undefined";
  }

  if (typeof email !== "string") {
    return " Error : Input should be string";
  }

  if (!/[@]/) {
    return " Error : Email should contain @ character";
  }

  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  if (emailRegex.test(email)) {
    return "Email valid";
  } else {
    return "Email invalid";
  }
}

console.log(checkEmail("nananan@gmail.com"));
console.log(checkEmail("nananan@binar.co.id"));
console.log(checkEmail("nananan@gmail"));
console.log(checkEmail("nananan.com"));
console.log(checkEmail("nananan"));
