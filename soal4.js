function isValidPassword(password) {
  if (typeof password == "undefined") {
    return `Error (Anda belum memasukan password)`;
  }
  if (typeof password != "string") {
    return `Error (Karena password ${password} yang diberikan tidak berupa string)`;
  }
  if (password.length < 8) {
    return `False (Karena password ${password} kurang dari 8)`;
  }
  if (!/[A-Z]/.test(password)) {
    return `False (Karena password ${password} tidak memiliki huruf besar)`;
  }
  if (!/[a-z]/.test(password)) {
    return `False (Karena password ${password} tidak memiliki huruf kecil)`;
  }
  if (!/[0-9]/.test(password)) {
    return `False (Karena password ${password} tidak memiliki angka)`;
  }
  return `True (Password sesuai)`;
}

console.log(isValidPassword("Meong2021"));
console.log(isValidPassword(0));
console.log(isValidPassword());
console.log(isValidPassword("meong"));
console.log(isValidPassword("meong2021"));
console.log(isValidPassword("M@eongghii"));
console.log(isValidPassword("MEONG2021"));
