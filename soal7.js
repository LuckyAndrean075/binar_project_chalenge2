const dataPenjualanPakAldi = [
  {
    namaProduct: "Sepatu Futsal Nike Vapor Academy 8",
    hargaSatuan: 760000,
    kategori: "Sepatu Sport",
    totalTerjual: 90,
  },
  {
    namaProduct: "Sepatu Warrior Tristan Black Brown High",
    hargaSatuan: 960000,
    kategori: "Sepatu Sneaker",
    totalTerjual: 37,
  },
  {
    namaProduct: "Sepatu Warrior Tristan Maroon High ",
    kategori: "Sepatu Sneaker",
    hargaSatuan: 360000,
    totalTerjual: 90,
  },
  {
    namaProduct: "Sepatu Warrior Rainbow Tosca Corduroy",
    hargaSatuan: 120000,
    kategori: "Sepatu Sneaker",
    totalTerjual: 90,
  },
];

// console.log(dataPenjualanPakAldi[0].totalTerjual);

function getInfoPenjualan(dataPenjualan) {
  if (!Array.isArray(dataPenjualan)) {
    return "Error input must be an array";
  }

  let jumlahPenjualan = 0;
  for (let i = 0; i < dataPenjualan.length; i++) {
    // console.log(dataPenjualan[i].totalTerjual);
    jumlahPenjualan += dataPenjualan[i].totalTerjual;
  }
  return jumlahPenjualan;
}

console.log(getInfoPenjualan(dataPenjualanPakAldi));
