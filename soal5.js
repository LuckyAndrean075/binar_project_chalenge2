function getSplitName(personName) {
  if (typeof personName === "undefined") {
    return "Error input is undefined";
  }
  if (typeof personName !== "string") {
    return "Error input is no string";
  }

  let result = personName.split(" ");
  if (result.length == 3) {
    return `firstName = ${result[0]},  middleName = ${result[1]}, lastName = ${result[2]}`;
  } else if (result.length == 2) {
    return `firstName = ${result[0]},  middleName = null, lastName = ${result[1]}`;
  } else if (result.length == 1) {
    return `firstName = ${result[0]},  middleName = null, lastName = null`;
  } else {
    return "This function is only for 1-3 characters name";
  }
}

console.log(getSplitName("Aldi Danella Pranata"));
console.log(getSplitName("Dwi Kuncoro"));
console.log(getSplitName("Aurora"));
console.log(getSplitName("Aurora Aurellia Sukma Dharma"));
console.log(getSplitName(0));
console.log(getSplitName());
