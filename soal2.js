function cekTypeNumber(number) {
  if (typeof number === "undefined") {
    return "Anda belum memasukkan parameter";
  } else if (number % 2 == 0) {
    return "Angka merupakan bilangan genap";
  } else if (number % 2 == 1) {
    return "Angka merupakan bilangan ganjil";
  } else {
    return "Parameter yang anda masukan salah";
  }
}

console.log(cekTypeNumber(11));
console.log(cekTypeNumber(28));
console.log(cekTypeNumber({}));
console.log(cekTypeNumber("dua"));
console.log(cekTypeNumber());
console.log(cekTypeNumber(13.8));
